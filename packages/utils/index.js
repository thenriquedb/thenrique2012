function welcome(name) {
  return `Welcome ${name} to utils-01!`;
}

function getDate() {
  return new Date().getDate().toString();
}

module.exports = {
  welcome,
  getDate,
};
