# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.5](https://gitlab.com/thenriquedb/thenrique2012/compare/v1.0.4...v1.0.5) (2021-10-27)

**Note:** Version bump only for package @thenrique2012/utils





## [1.0.4](https://gitlab.com/thenriquedb/thenrique2012/compare/v1.0.3...v1.0.4) (2021-10-27)

**Note:** Version bump only for package @thenrique2012/utils





## [1.0.3](https://gitlab.com/thenriquedb/thenrique2012/compare/v1.0.2...v1.0.3) (2021-10-27)

**Note:** Version bump only for package @thenrique2012/utils







**Note:** Version bump only for package @thenrique2012/utils





## [1.0.1](https://gitlab.com/thenriquedb/thenrique2012/compare/v1.0.2...v1.0.1) (2021-10-27)

**Note:** Version bump only for package @thenrique2012/utils
