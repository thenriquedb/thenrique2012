function welcomeUtils2(name) {
  return `Welcome ${name} to utils 2!`;
}

function getDateUtils2() {
  console.log("utils 2  ");
  return new Date().getDate().toString();
}

module.exports = {
  welcomeUtils2,
  getDateUtils2,
};
